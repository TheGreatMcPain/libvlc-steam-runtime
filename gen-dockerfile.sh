#!/bin/sh

UID=$(id -u)
GID=$(id -g)
UNAME=$(whoami)

cat << EOF > Dockerfile
FROM registry.gitlab.steamos.cloud/steamrt/scout/sdk:latest
RUN mkdir /home
RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -G sudo -g $GID -s /bin/bash $UNAME
RUN echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
COPY . /usr/src/vlc-build
RUN chown -Rv $UNAME:$UNAME /usr/src/vlc-build
WORKDIR /usr/src/vlc-build
USER $UNAME
EOF
