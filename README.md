# libvlc built against steam-runtime (scout)

Compile vlc against steam-runtime's SDK.

## Requirements

docker, with docker-compose.

## Usage

First run `gen-dockerfile.sh` to create the `Dockerfile`.

(The Dockerfile recreates the host's user within the docker container.)

```bash
$ ./gen-dockerfile.sh
```

Then setup and run the docker container with docker-compose.

```bash
$ docker-compose run --rm vlc-build
```

Once inside the container's shell simply run `vlc-steam-runtime-build.sh`, cross your fingers, maybe grab a beverage, and wait for it to finish.

```bash
$ ./vlc-steam-runtime-build.sh
```

Once complete the `vlc-prefix` folder should contain an installation of `vlc` that's suitable for the UE4 plugin VlcMedia.

If you're using this with VlcMedia just copy the contents of `vlc-prefix` into the below directory.

```
/path/to/VlcMedia/ThirdParty/vlc/Linux/x86_64-unknown-linux-gnu
```
