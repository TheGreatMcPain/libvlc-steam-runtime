#!/bin/sh
# Build vlc against stream-runtime
# Designed to be ran inside steam-runtime scout docker.
# (Mainly created for UE4 VlcMedia plugin)

INSTALL_DIR="$(pwd)/vlc-prefix"
OLD_PWD="$(pwd)"
# rm -rfv "$INSTALL_DIR"
# mkdir "$INSTALL_DIR"

apt_install_deps() {
  # Update apt-get repositories
  sudo apt-get update
  
  # Install vlc deps
  sudo apt-get -y install git build-essential pkg-config libtool automake autopoint gettext bison
  
  # Install contrib deps
  sudo apt-get -y install install-info # required for gperf
  sudo apt-get -y install subversion yasm cmake ragel ant default-jdk flex gperf gengetopt
}

# Install protobuf, python3.6, and nasm from source
# (scout is based on ubuntu 12.04 which doesn't include new enough packages.)
install_protobuf() {
  # protobuf
  if ! [ -d protobuf ]; then
    git clone git://github.com/google/protobuf.git
    cd protobuf
    git checkout -b 3.17.3 v3.17.3
    git submodule update --init --recursive
  else
    cd protobuf
  fi
  ./autogen.sh
  ./configure
  make -j$(nproc)
  # make check # verify that it works.
  sudo make install
  # make clean
  sudo ldconfig
  cd $OLD_PWD
}

install_nasm() {
  # nasm
  if ! [ -d nasm ]; then
    git clone https://repo.or.cz/nasm.git
    cd nasm
    git checkout -b 2.4.02 nasm-2.14.02
  else
    cd nasm
  fi
  ./autogen.sh
  ./configure
  make -j$(nproc)
  sudo make install
  # make clean
  cd $OLD_PWD
}

install_python36() {
  # python3.6
  if ! [ -d cpython ]; then
    git clone https://github.com/python/cpython.git
    cd cpython
    git checkout -b 3.6.14 v3.6.14
  else
    cd cpython
  fi
  ./configure
  make -j$(nproc)
  sudo make altinstall
  # make clean
  cd $OLD_PWD
}

install_meson() {
  # Install updated meson via python3.6
  sudo python3.6 -m pip install meson
}

clone_vlc() {
  # Clone vlc sources
  if ! [ -d vlc ]; then
    git clone https://code.videolan.org/videolan/vlc.git
    cd vlc
    git checkout -b 3.0.16 3.0.16
  else
    cd vlc
  fi

  cd $OLD_PWD
}

apply_vlc_patches() {
  cd vlc
  # Apply gen-meson-crossfile.py patch (Forces use of python3.6)
  patch -Np1 -i ../0001-contrib-Force-python3.6-for-gen-meson-crossfile.py.patch
  # Apply PRId64 fixes
  patch -Np1 -i ../0001-Fix-PRId64-errors.patch
  cd $OLD_PWD
}

bootstrap_vlc() {
  cd vlc
  ./bootstrap || return
  cd $OLD_PWD
}

setup_vlc_contrib() {
  cd vlc/contrib
  mkdir native
  cd native
  ../bootstrap || return

  # Compile gcrypt first using gcc-5 (doesn't like gcc-9)
  CC="gcc-5" CXX="g++-5" make .gcrypt

  # Compile rest of the libraries
  make -j$(nproc) || return

  cd $OLD_PWD
}

# Compile static libidn (the one provided by ubuntu 12.04 doesn't seem to work)
# (since vlc and libidn are both LGPL I think they can be statically linked)
add_libidn_to_vlc_contrib() {
  git clone git://git.savannah.gnu.org/libidn.git
  if ! [ -d libidn ]; then
    git clone git://git.savannah.gnu.org/libidn.git
    cd libidn
    git checkout -b 1.36 libidn-1-36
  else
    cd libidn
  fi
  ./bootstrap
  ./configure \
    --with-pic \
    --disable-shared \
    --enable-static \
    --disable-doc \
    --prefix=$(realpath ../vlc/contrib/x86_64-linux-gnu)
  make -j$(nproc)
  make install
  # make clean
  cd $OLD_PWD
}

configure_vlc() {
  cd vlc
  ./configure \
    --disable-srt \
    --disable-dca \
    --disable-libass \
    --disable-css \
    --disable-upnp \
    --disable-chromaprint \
    --disable-freetype \
    --disable-chromecast \
    --prefix=$INSTALL_DIR || return
    
  make -j$(nproc)
  make install

  cd $OLD_PWD
}

apt_install_deps
install_protobuf
install_nasm
install_python36
install_meson

clone_vlc
apply_vlc_patches

# Start using gcc-9 instead of the default gcc-4.8
# Normally I'd use gcc-9 for the software above, but I found that
# some programs will crash if compiled with it. (esp. Protobuf)
#
# Looks like this version of gcc-9 is monolithic,
# and was built by Valve for this version of stream-runtime.
export CC="gcc-9"
export CXX="g++-9"

bootstrap_vlc
setup_vlc_contrib
add_libidn_to_vlc_contrib
configure_vlc
